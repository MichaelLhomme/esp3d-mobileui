import { boot } from 'quasar/wrappers';
import Portal from 'vue3-portal';

export default boot(({ app }) => {
  app.use(Portal);
});
