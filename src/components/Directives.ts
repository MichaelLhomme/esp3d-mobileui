import { useVibrate } from '@vueuse/core';

const { vibrate } = useVibrate({ pattern: 25 });

const hapticHandler = (_e: Event) => vibrate();

export const vHaptic = (el: Element) =>
  el.addEventListener('click', hapticHandler);
