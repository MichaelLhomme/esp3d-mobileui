import { Notify } from 'quasar';

export const notifySuccess = (msg: string) => {
  Notify.create({
    message: msg,
    type: 'positive',
    timeout: 1000,
  });
};

export const notifyWarning = (msg: string) => {
  Notify.create({
    message: msg,
    type: 'warning',
    timeout: 2000,
  });
};

export const notifyError = (msg: string, err: unknown) => {
  Notify.create({
    message: msg,
    caption: `${err}`,
    type: 'negative',
    timeout: 3000,
  });
};
