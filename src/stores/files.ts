import { defineStore } from 'pinia';

import {
  File,
  FileSystemStatus,
  ListResponse,
  listDirectory,
} from 'src/fluidnc/files';

export const useFilesStore = defineStore('files', {
  state: () => ({
    status: undefined as FileSystemStatus | undefined,
    files: {} as Record<string, File[]>,
    loading: false,
  }),

  getters: {},

  actions: {
    clear() {
      this.$patch((state) => {
        state.status = undefined;
        state.files = {};
        state.loading = false;
      });
    },

    async fetchFilesIfNeeded(path: string): Promise<void> {
      if (!this.files[path]) return this.fetchFiles(path);
    },

    async fetchFiles(path: string): Promise<void> {
      this.loading = true;
      return listDirectory(path)
        .then((res: ListResponse) => {
          this.$patch((state) => {
            state.status = {
              occupation: res.occupation,
              total: res.total,
              used: res.used,
            };

            state.files[path] = res.files;

            state.loading = false;
          });
        })
        .catch((err) => {
          this.loading = false;
          throw err;
        });
    },
  },
});
