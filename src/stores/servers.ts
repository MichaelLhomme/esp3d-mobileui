import { defineStore } from 'pinia';

import { useFilesStore } from './files';
import { Server } from 'src/fluidnc/server';
import { useStatusStore } from './status';

export const useServersStore = defineStore('servers', {
  state: () => ({
    currentId: undefined as string | undefined,
    servers: {} as Record<string, Server>,
  }),

  getters: {
    currentServer: (state) =>
      state.currentId ? state.servers[state.currentId] : undefined,

    serversList: (state) =>
      Object.values(state.servers).sort((a, b) => a.name.localeCompare(b.name)),
  },

  actions: {
    createServer(server: Server) {
      console.log(`Creating server ${server.name}`);
      this.servers[server.id] = { ...server };
    },

    updateServer(server: Server) {
      console.log(`Updating server ${server.name}`);
      this.servers[server.id] = { ...server };
    },

    deleteServer(server: Server) {
      console.log(`Deleting server ${server.name}`);
      delete this.servers[server.id];
    },

    setCurrentServer(server: Server) {
      console.log(`Connecting to ${server.name}`);
      this.currentId = server.id;
      useFilesStore().clear();
      useStatusStore().connect();
      this.router.push('/jog');
    },

    updateSpeed(speedXY: number, speedZ: number) {
      if (!this.currentServer) return;
      this.currentServer.speed = { speedXY, speedZ };
    },
  },

  persist: {
    enabled: true,
    strategies: [
      {
        storage: localStorage,
        paths: ['servers'],
      },
      {
        storage: sessionStorage,
        paths: ['currentId'],
      },
    ],
  },
});
