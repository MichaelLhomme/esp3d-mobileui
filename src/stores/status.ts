import { defineStore } from 'pinia';

import { notifyError, notifySuccess } from 'components/notifications';

import { useFluidNCWebSocket } from 'src/fluidnc/websocket';
import { Macro, fetchMacros } from 'src/fluidnc/macros';
import { useServersStore } from './servers';

export const useStatusStore = defineStore('status', {
  state: () => ({
    connected: false,
    connecting: false,
    hostname: undefined as string | undefined,
    pageId: undefined as number | undefined,
    macros: [] as Macro[],
    position: {
      x: 0,
      y: 0,
      z: 0,
    },
  }),

  getters: {
    httpUrl: (state) => (path: string) => {
      if (!state.connected)
        throw new Error('Server not connected, cannot generate url');

      const base = `http://${state.hostname}/${path}`;
      return state.pageId !== undefined
        ? `${base}&PAGEID=${state.pageId}`
        : base;
    },
  },

  actions: {
    connect() {
      const currentServer = useServersStore().currentServer;
      if (!currentServer) throw new Error('No server selected, cannot connect');

      if (this.connected) this.disconnect();

      this.connecting = true;
      useFluidNCWebSocket().connect();
    },

    setConnected(hostname: string) {
      this.$patch({
        connected: true,
        connecting: false,
        hostname: hostname,
      });

      notifySuccess('Serveur connecté');
      fetchMacros().then((macros) => this.$patch({ macros }));
    },

    disconnect() {
      useFluidNCWebSocket().disconnect();

      this.$patch({
        connected: false,
        connecting: false,
        pageId: undefined,
        hostname: undefined,
        position: { x: 0, y: 0, z: 0 },
        macros: [],
      });

      notifyError('Serveur déconnecté', undefined);
    },

    setPageId(pageId: number) {
      this.pageId = pageId;
    },

    updatePosition(x: number, y: number, z: number) {
      this.$patch({
        position: {
          x,
          y,
          z,
        },
      });
    },
  },
});
