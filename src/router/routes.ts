import { RouteRecordRaw } from 'vue-router';

const _x = 1;

const routes: RouteRecordRaw[] = [
  {
    path: '/',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      { path: '/jog', component: () => import('src/pages/JogPage.vue') },
      { path: '/files', component: () => import('pages/FilesPage.vue') },
      { path: '/servers', component: () => import('pages/ServersPage.vue') },
    ],
  },

  // Always leave this as last one,
  // but you can also remove it
  {
    path: '/:catchAll(.*)*',
    component: () => import('pages/ErrorNotFound.vue'),
  },
];

export default routes;
