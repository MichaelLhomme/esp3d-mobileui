import { useStatusStore } from 'stores/status';

export interface SpeedConfiguration {
  speedXY: number;
  speedZ: number;
}

export interface Server {
  id: string;
  name: string;
  hostname: string;
  speed?: SpeedConfiguration;
}

export function serverCommand(path: string): Promise<Response> {
  const statusStore = useStatusStore();
  if (!statusStore.connected)
    throw new Error('No server connected, cannot send command');

  return fetch(statusStore.httpUrl(path));
}
