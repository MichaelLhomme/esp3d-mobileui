import { serverCommand } from './server';

export interface Macro_old {
  id: string;
  label: string;
  icon: string;
  fullPath: string;
}

export interface Macro {
  name: string;
  glyph: string;
  filename: string;
  target: string;
  class: string;
  index: number;
}

export const fetchMacros = async (): Promise<Macro[]> => {
  console.log('Listing macros');
  return serverCommand('macrocfg.json')
    .then((res) => res.json())
    .then((macros: Macro[]) =>
      macros.filter((macro) => macro.filename?.length > 0)
    );
};

export const runMacro = async (macro: Macro) => {
  console.log(`Running macro ${macro.name}`);
  serverCommand(`command?commandText=$SD/Run=${macro.filename}`);
};
