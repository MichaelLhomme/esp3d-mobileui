import { useServersStore } from 'stores/servers';

import { serverCommand } from './server';

export interface FileSystemStatus {
  occupation: string;
  total: string;
  used: string;
}
export interface File {
  name: string;
  shortname: string;
  size: string;
  datetime: string;
  fullPath: string;
}

export interface ListResponse extends FileSystemStatus {
  files: File[];
  path: string;
  status: string;
}

export const isDirectory = (file: File) => file.size === '-1';

export const listDirectory = async (path: string): Promise<ListResponse> => {
  const server = useServersStore().currentServer;
  if (!server) throw new Error('Cannot list directory, no server connected');

  return serverCommand(`upload?path=${path}`)
    .then((res) => res.json())
    .then((listRes: ListResponse) => {
      const newFiles = listRes.files.map((file) => ({
        ...file,
        fullPath: `${path === '/' ? '' : path}/${file.name}`,
      }));

      return {
        ...listRes,
        files: newFiles,
      };
    });
};

export const runFile = async (file: File) => {
  console.log(`Running file ${file.name}'...`);
  return serverCommand(`command?commandText=$SD/Run=${file.fullPath}`);
};
