import { useServersStore } from 'stores/servers';

import { serverCommand, SpeedConfiguration } from './server';

const motionCommand = async (cmd: string): Promise<void> => {
  console.log('Sending motion command', cmd);
  return serverCommand(`command?commandText=${encodeURIComponent(cmd)}`).then(
    () => {
      return; // For Promise<void> return type
    }
  );
};

const getSpeed = (): SpeedConfiguration => {
  const speed = useServersStore().currentServer?.speed;
  if (!speed) throw new Error('Missing speed configuration');
  return speed;
};

export const homing = async () => {
  return motionCommand('$H');
};

export const homingByAxis = async (axis: string) => {
  return motionCommand(`$H${axis}`);
};

export const jogXY = async (x: number | undefined, y: number | undefined) => {
  const speed = getSpeed();
  if (x === undefined && y === undefined) {
    console.warn('Missing jog distances');
    return;
  }

  let cmd = `$J=G91 G21 F${speed.speedXY} `;
  if (x !== undefined) cmd += `X${x}`;
  if (y !== undefined) cmd += `Y${y}`;

  return motionCommand(cmd);
};

export const jogZ = async (z: number) => {
  const speed = getSpeed();
  const cmd = `$J=G91 G21 F${speed.speedZ} Z${z}`;
  return motionCommand(cmd);
};
