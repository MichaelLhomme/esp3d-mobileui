import { toString } from 'uint8arrays/to-string';

import { useStatusStore } from 'stores/status';
import { useServersStore } from 'stores/servers';

import { notifySuccess } from 'components/notifications';

import { serverCommand } from './server';

class FluidNCWebSocket {
  private ws: WebSocket | undefined;
  private timer: NodeJS.Timeout | undefined;

  public disconnect() {
    clearInterval(this.timer);
    if (this.ws) this.ws.close();
    this.ws = undefined;
  }

  public connect() {
    const server = useServersStore().currentServer;
    if (!server)
      throw new Error('No server selected, cannot connect Websocket');

    try {
      console.log('Opening WebSocket...');

      //this.ws_source = new WebSocket('ws://' + server.url + '/ws', ['arduino']);
      this.ws = new WebSocket('ws://' + server.hostname + ':' + 81, [
        'arduino',
      ]);
      this.ws.binaryType = 'arraybuffer';

      this.ws.onopen = (e) => this.onConnected(server.hostname, e);
      this.ws.onclose = (e) => this.onClose(e);
      this.ws.onerror = (e) => this.onError(e);
      this.ws.onmessage = (e) => this.onMessage(e);
    } catch (exception) {
      console.error(exception);
    }
  }

  private onConnected(hostname: string, _e: Event) {
    console.log('WebSocket Connected');
    useStatusStore().setConnected(hostname);

    this.timer = setInterval(
      () => serverCommand('command?commandText=?'),
      3000
    );
  }

  private onClose(_e: CloseEvent) {
    console.log('WebSocket Disconnected');
    clearInterval(this.timer);
    useStatusStore().disconnect();

    //seems sometimes it disconnect so wait 3s and reconnect
    //if it is not a log off
    //if (!log_off) setTimeout(startSocket, 3000);
  }

  private onError(e: Event) {
    console.error('WS error', e);
  }

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  private onMessage(e: MessageEvent<any>) {
    if (e.data instanceof ArrayBuffer) {
      const buffer = toString(new Uint8Array(e.data));
      buffer
        .split('\n')
        .filter((msg) => msg !== '')
        .forEach((msg) => this.onBufferMessage(msg));
    } else if (typeof e.data === 'string') {
      this.onSimpleMessage(e.data);
    }
  }

  private onSimpleMessage(msg: string) {
    if (msg.startsWith('CURRENT_ID:')) {
      this.onCurrentID(msg);
    } else if (msg.startsWith('ACTIVE_ID:')) {
      // Something to do ?
    } else if (msg.startsWith('PING:')) {
      // TODO implement heart beat for disconnection ?
    } else {
      console.warn('Unknown simple message', msg);
    }
  }

  private onBufferMessage(msg: string) {
    if (msg === 'ok') {
      // TODO implement persistent notifications discarded when ok is received (FIFO)
      console.log('Received command ok');
    } else if (msg === '[MSG:INFO: Program End]') {
      notifySuccess('Program end !');
    } else if (msg.match(/^<.*\|/)) {
      this.onStatusUpdate(msg);
    } else if (msg.match(/^\[MSG:.*file job succeeded\]$/)) {
      notifySuccess('File processed !');
    } else {
      console.warn(`Unknown buffer message '${msg}'`);
    }
  }

  private onStatusUpdate(status: string) {
    // <Run|MPos:83.655,43.995,-17.000|FS:1000,0|Pn:P|Ov:100,100,100|A:S|SD:30.27,/sd/laser/file.gcode>
    console.log('Updating status', status);
    const tmp = status.split('|')[1].split(':')[1];
    const positions = tmp.split(',');

    useStatusStore().updatePosition(
      parseFloat(positions[0]),
      parseFloat(positions[1]),
      parseFloat(positions[2])
    );
  }

  private onCurrentID(msg: string) {
    const tmp = msg.split(':');
    const pageId = parseInt(tmp[1]);

    console.log('Setting pageId', pageId);
    useStatusStore().setPageId(pageId);
  }
}

const socket = new FluidNCWebSocket();

export const useFluidNCWebSocket = () => socket;
